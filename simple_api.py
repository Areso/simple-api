#!/usr/bin/env python3
from flask import Flask, jsonify
from flask import request
from flask_sockets import Sockets
import json

app = Flask(__name__)
sockets = Sockets(app)


@app.route('/hello', methods=['GET', 'POST', 'OPTIONS'])
def hello():
    code = 200
    msg = "hello world bi turbo 21-01 via shell "
    return {"msg": msg}, code, {"Access-Control-Allow-Origin": "*",
                                "Content-type": "application/json",
                                "Access-Control-Allow-Methods": ["GET", "POST"]}


if __name__ == "__main__":
    print('run web server')
    app.debug = True
    app.run(host='0.0.0.0', port=5015)
